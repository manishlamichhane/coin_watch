# coinwatch version 1.0.0


# Steps to use:
1. Clone the repo
2. run `pip install -r requirements.txt`
3. rename `investments.json`.example to `investments.json`
4. Update investments.json with your investment details. Following is the definition of each term in investments.json file
	- amount:  The total fiat you invested for this particular buy
	- purchase_rate: The purchase_rate at the time of buy
	If you purchased BTC multiple times, add each investment in the provided format. 
		{
			"amount": <Amount>,
			"purchase_rate" : <Purchase Rate>
		}
	Important: DO NOT append a trailing comma (,) after the last entry. This will invalidate the json file.
	Also, use single quotes ('') to represent strings
5. To run: `python app.py` or `python profitcalculator.py`

# TODO:
- Easier way to add investment without editing investments.json manually
- Dynamically read currencies and get the appropriate rates from API
- Hit me up with others 

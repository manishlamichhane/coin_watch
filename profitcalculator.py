from __future__ import print_function 

import requests
import json
import sys
from colorprint import *


class ProfitCalculator:

	INVESTMENTS_FILE = 'investments.json'
	API_URL = 'https://api.coinbase.com/v2/exchange-rates?currency='
	CONVERSION_CURRENCY = 'USD'

	def __init__(self):
		self.investments_data = {}
		self._load_investments()
		self.currency_list = []
		self._parse_currencies_from_json()
		self.rate = {}
		self._get_current_exchange_rates()
		self.total_investment, self.rev, self.profit = 0, 0, 0
		self._calculate()
		self._display()

	def _load_investments(self):
		try:
			with open(self.INVESTMENTS_FILE) as json_data:
				self.investments_data = json.load(json_data)
		except IOError:
			print('Please rename the investments.json.example file to investments.json '
				'and update it with your investments info.', color='red', format=['bold', 'blink'])
			sys.exit()
		except ValueError as e:
			print('The JSON data in investments.json is invalid. You can use:', color='red', format=['bold', 'blink'])
			print('https://jsonlint.com/ to validate JSON', color='green', format=['bold', 'blink'])
			sys.exit()

	def _parse_currencies_from_json(self):
		self.currency_list = [[currency, self._create_api_url(currency)]  for currency in self.investments_data]

	def _create_api_url(self, currency):
		return self.API_URL + currency

	def _get_current_exchange_rates(self):
		error_counter = 0
		for currency in self.currency_list:
			currency_name, currency_url = currency[0], currency[1]
			try:
				rate_json = requests.get(currency_url).content
				rate_float = float(json.loads(rate_json)['data']['rates'][self.CONVERSION_CURRENCY])
				self.rate[currency_name] = rate_float
			except ValueError:
				error_counter = error_counter + 1
				print('The currency {} could not be identified by API. '\
					'It will be excluded from calculations. '.format(currency_name))
				pass
		if error_counter == len(self.currency_list):
			sys.exit()


	def _calculate(self):
		for currency in self.investments_data:
			for details in self.investments_data[currency]:
				details['individual_revenue'] = ((self.rate[currency] / details['purchase_rate']) * details['amount'])
				self.total_investment = self.total_investment + details['amount']
				self.rev = self.rev + details['individual_revenue']
		self.profit = self.rev - self.total_investment


	def _display(self):
		print('PROFIT: {} {}'.format(self.profit, self.CONVERSION_CURRENCY), color='red', format=['bold', 'blink'])
		print('Investment: {} {}'.format(self.total_investment,  self.CONVERSION_CURRENCY))
		print('Rates: ', self.rate)
		print('Revenue: {} {}'.format(self.rev, self.CONVERSION_CURRENCY))
		
		for currency in self.investments_data:
			total_individual_revenue = 0
			for details in self.investments_data[currency]:
				total_individual_revenue = total_individual_revenue + details['individual_revenue']
			print("Revenue in {}: {} {}".format(currency, total_individual_revenue, self.CONVERSION_CURRENCY))


ProfitCalculator()